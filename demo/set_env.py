from pathlib import Path
import os
import numpy as np


def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True

home = Path(os.getcwd())
p_data = home / 'data'
p_model = home / 'models'

if module_exists('dnc_set_env'):
    import dnc_set_env
    home = dnc_set_env.home()
elif home.exists():
    print('')  # default case
else:
    raise Exception('Local directory not found?')


def raw_path(str_sub, run_id):
    p_raw = p_data.joinpath('raw_data').joinpath(str_sub)
    f_raw = '{}_r0{}_rs.set'.format(str_sub, run_id)
    return p_raw, f_raw


def obs_path(str_sub, run_id):
    p_bcg = p_data.joinpath('obs_data').joinpath(str_sub)
    f_bcg = '{}_r0{}_rmbcg.set'.format(str_sub, run_id)
    return p_bcg, f_bcg


def arch_path(str_sub, run_id):
    if not isinstance(run_id, (list, tuple, np.ndarray)):
        p_arch = p_model.joinpath('trained').joinpath(str_sub).joinpath('r0{}'.format(run_id))
    else:
        run_id_str = ''.join(str(x) for x in run_id)
        p_arch = p_model.joinpath('trained').joinpath(str_sub).joinpath('r0{}'.format(run_id_str))

    return p_arch


def output_path(str_sub, run_id, arch, opt=None):
    opt_local = opt

    if opt_local.multi_run:
        p_out = p_data.joinpath('data/cleaned_data/proc_bcgnet_mat/multi_run').joinpath(arch).joinpath(str_sub)
        f_out = '{}_r0{}_bcgnet.mat'.format(str_sub, run_id)
    else:
        p_out = p_data.joinpath('data/cleaned_data/proc_bcgnet_mat/single_run').joinpath(arch).joinpath(str_sub)
        f_out = '{}_r0{}_bcgnet.mat'.format(str_sub, run_id)

    return p_out, f_out


def figure_path(fignum):
    p_figure = p_data.joinpath(str_experiment).joinpath('proc_full').joinpath('figures').joinpath('fig0{}'.format(fignum))

    return p_figure