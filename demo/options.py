class TrainDefault:
    """
    Class used for initializing hyperparameters of the training
    Values here correspond to the values used in the actual paper

    Training parameters:
    _epochs: number of epochs: int
    _training: percentage to be used as the training set: float
    _validation: percentage to be used as the validation set: float
    _multi_run: flag for whether training is performed on multiple runs from same subject: boolean
    _multi_sub: flag for whether or not you are training on multiple subjects: boolean
    _training_dataset_gen: whether to generate the dataset during training: boolean
    _training_figure_gen: whether to generate the figures during training: boolean
    _training_figure_num: number of intermediate figures to generate during training: int

    Loading pre-trained model parameters:
    _resume: whether resuming the training of a particular model: boolean
    _p_arch: path of the architecture (mostly as a place holder): pathlib.Path
    _f_arch: name of the architecture, have to be named in the case of resuming or evaluating: pathlib.Path

    Early stopping parameters:
    _early_stopping: flag for turning on the early stopping feature: boolean
    _es_min_delta:  minimal change to be considered as an improvement by Keras: float
    _es_patience: patience factor for early stopping in Keras: int

    Evaluation mode parameters:
    _evaluate_model: flag for turning on the evaluation feature: boolean
    _evaluation_dataset_gen: whether to generate the dataset during evaluation: boolean
    _evaluation_figure_gen: whether to generate figures while evaluating the model: boolean
    _evaluation_figure_num: number of intermediate figures to generate while evaluating the model: int
    """

    def __init__(self):
        # Training parameters
        self._epochs = 2500
        self._training = 0.7
        self._validation = 0.15
        self._multi_run = True
        self._multi_sub = False
        self._training_dataset_gen = True
        self._training_figure_gen = False
        self._training_figure_num = 0

        # Using pre-trained model parameters
        self._resume = False
        self._p_arch = None
        self._f_arch = None

        # Early stopping parameters
        self._early_stopping = True
        self._es_min_delta = 1e-5
        self._es_patience = 25

        # Evaluation mode parameter
        self._evaluate_model = False
        self._evaluation_dataset_gen = False
        self._evaluation_figure_gen = False
        self._evaluation_figure_num = 0

    # Defining all the properties
    # Training parameters
    @property
    def epochs(self):
        return self._epochs

    @property
    def training(self):
        training = self._training
        if training is not None:
            if training >= 1: raise Exception('percentage of training cannot be greater than 1')
            if training == 0: evaluation = None

        return training

    @property
    def validation(self):
        validation = self._validation
        if validation is not None:
            if validation >= 1: raise Exception('validation cannot be greater than 1')
            if validation == 0: validation = None

        return validation

    @property
    def multi_run(self):
        return self._multi_run

    @property
    def multi_sub(self):
        return self._multi_sub

    @property
    def training_dataset_gen(self):
        return self._training_dataset_gen

    @property
    def training_figure_gen(self):
        return self._training_figure_gen

    @property
    def training_figure_num(self):
        return self._training_figure_num

    # Using pre-trained model parameters
    @property
    def resume(self):
        return self._resume

    @property
    def p_arch(self):
        return self._p_arch

    @property
    def f_arch(self):
        return self._f_arch

    # Early stopping parameters
    @property
    def early_stopping(self):
        return self._early_stopping

    @property
    def es_min_delta(self):
        return self._es_min_delta

    @property
    def es_patience(self):
        return self._es_patience

    # Evaluation mode parameter
    @property
    def evaluate_model(self):
        return self._evaluate_model

    @property
    def evaluation_dataset_gen(self):
        return self._evaluation_dataset_gen

    @property
    def evaluation_figure_gen(self):
        return self._evaluation_figure_gen

    @property
    def evaluation_figure_num(self):
        return self._evaluation_figure_num

    # Defining all the mutator methods
    # Training parameters
    @epochs.setter
    def epochs(self, value):
        self._epochs = value

    @training.setter
    def training(self, value):
        self._training = value

    @validation.setter
    def validation(self, value):
        self._validation = value

    @multi_run.setter
    def multi_run(self, value):
        self._multi_run = value

    @multi_sub.setter
    def multi_sub(self, value):
        self._multi_sub = value

    @training_dataset_gen.setter
    def training_dataset_gen(self, value):
        self._training_dataset_gen = value

    @training_figure_gen.setter
    def training_figure_gen(self, value):
        self._training_figure_gen = value

    @training_figure_num.setter
    def training_figure_num(self, value):
        self._training_figure_num = value

    # Using pre-trained model parameters
    @resume.setter
    def resume(self, value):
        self._resume = value

    @p_arch.setter
    def p_arch(self, value):
        self._p_arch = value

    @f_arch.setter
    def f_arch(self, value):
        self._f_arch = value

    # Early stopping parameters
    @early_stopping.setter
    def early_stopping(self, value):
        self._early_stopping = value

    @es_min_delta.setter
    def es_min_delta(self, value):
        self._es_min_delta = value

    @es_patience.setter
    def es_patience(self, value):
        self._es_patience = value

    # Evaluation mode parameter
    @evaluate_model.setter
    def evaluate_model(self, value):
        self._evaluate_model = value

    @evaluation_dataset_gen.setter
    def evaluation_dataset_gen(self, value):
        self._evaluation_dataset_gen = value

    @evaluation_figure_gen.setter
    def evaluation_figure_gen(self, value):
        self._evaluation_figure_gen = value

    @evaluation_figure_num.setter
    def evaluation_figure_num(self, value):
        self._evaluation_figure_num = value
