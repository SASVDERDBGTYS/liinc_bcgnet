import numpy as np
import mne


def normalize_raw_data(raw_data):
    # Assuming that raw data is an mne Raw object
    data = raw_data.get_data()
    info = raw_data.info

    ecg_ch = info['ch_names'].index('ECG')
    target_ch = np.delete(np.arange(0, len(info['ch_names']), 1), ecg_ch)

    # used for reverting back to original data later
    ecg_mean = np.mean(data[ecg_ch, :])
    ecg_std = np.std(data[ecg_ch, :])
    eeg_mean = np.mean(data[target_ch, :], axis=1)
    eeg_std = np.std(data[target_ch, :], axis=1)

    normalized_data = np.zeros(data.shape)
    for i in range(data.shape[0]):
        ds = data[i, :] - np.mean(data[i, :])
        ds /= np.std(ds)
        normalized_data[i, :] = ds

    normalized_raw = mne.io.RawArray(normalized_data, info)

    ecg_stats = [ecg_mean, ecg_std]
    eeg_stats = [eeg_mean, eeg_std]

    return normalized_raw, ecg_stats, eeg_stats


def renormalize(data, stats, multi_ch, multi_run=False, vec_run_id=None):
    if not multi_run:
        if not multi_ch:
            data_renorm = data*stats[1] + stats[0]
        else:
            data_renorm = np.zeros(data.shape)
            # If time series
            if len(data.shape) == 2:
                for i in range(data.shape[0]):
                    data_renorm[i, :] = data[i, :] * stats[1][i] + stats[0][i]
            # If epoch data
            else:
                for i in range(data.shape[0]):
                    data_renorm[i, :, :] = data[i, :, :] * stats[1][i] + stats[0][i]
    else:
        if not multi_ch:
            # For single channel epoch data
            vec_stats = stats
            data_renorm = np.zeros(data.shape)
            for i in range(data.shape[0]):
                data_renorm[i, :] = data[i, :] * vec_stats[vec_run_id[i] - 1][1] + vec_stats[vec_run_id[i] - 1][0]
        else:
            vec_stats = stats
            data_renorm = np.zeros(data.shape)
            for i in range(data.shape[1]):
                sub_stats = np.array(vec_stats[vec_run_id[i] - 1])
                sub_avg = np.repeat(sub_stats[0, :][:, np.newaxis], data.shape[2], axis=1)
                sub_std = sub_stats[1, :]
                data_renorm[:, i, :] = data[:, i, :] * sub_std[:, np.newaxis] + sub_avg

    return data_renorm