import scipy.stats as stats
from demo.utils.sp.sp_normalization import *
from demo.utils.context_management import suppress_stdout


def preprocessing(dataset_dir):
    # Loading the raw input in EEGLAB format and downsampling it
    rs_raw = mne.io.read_raw_eeglab(dataset_dir, preload=True, stim_channel=False)
    srate = rs_raw.info['sfreq']
    fs = srate / 5
    rs_raw.resample(fs)

    rs_data = rs_raw.get_data()
    normalized_raw, ecg_stats, eeg_stats = normalize_raw_data(rs_data)

    epoched_data, good_ix = dataset_epoch(dataset=normalized_raw, duration=3, epoch_rejection=True,
                                          threshold=5, raw_dataset=rs_raw)

    return normalized_raw, ecg_stats, eeg_stats, epoched_data, good_ix


def epoch_events(dataset, fs, duration):
    total_time_stamps = dataset.get_data().shape[1]
    constructed_events = np.zeros(shape=(int(np.floor(total_time_stamps/fs)/duration), 3), dtype=int)

    for i in range(0, int(np.floor(total_time_stamps/fs))-duration, duration):
        ix = i/duration
        constructed_events[int(ix)] = np.array([i*fs, 0, 1])

    tmax = duration - 1/fs

    return constructed_events, tmax


def mad_rejection(dataset, threshold, fs, duration):
    with suppress_stdout():
        srate = dataset.info['sfreq']
        if srate != fs:
            dataset.resample(fs)

        info = dataset.info
        ecg_ch = info['ch_names'].index('ECG')
        target_ch = np.delete(np.arange(0, len(info['ch_names']), 1), ecg_ch)

        constructed_events, tmax = epoch_events(dataset, fs, duration)
        epoched_dataset = mne.Epochs(dataset, constructed_events, tmin=0, tmax=tmax)
        data_abs = np.absolute(epoched_dataset.get_data())

    vec_mabs_eeg = np.mean(data_abs[:, target_ch, :], axis=(1, 2))
    vec_eeg_norm = (vec_mabs_eeg - np.median(vec_mabs_eeg)) / stats.median_absolute_deviation(vec_mabs_eeg)
    vec_bad_epochs_ix = np.arange(0, len(vec_eeg_norm), 1)[vec_eeg_norm > threshold]

    return vec_bad_epochs_ix


def dataset_epoch(dataset, duration, epoch_rejection, threshold=None, raw_dataset=None, good_ix=None):
    # Constructing events of duration 10s
    info = dataset.info
    fs = info['sfreq']

    constructed_events, tmax = epoch_events(dataset, fs, duration)
    old_epoched_data = mne.Epochs(dataset, constructed_events, tmin=0, tmax=tmax)

    if epoch_rejection:
        # Epoch rejection based on median absolute deviation of mean of absolute values for individual epochs
        ix = mad_rejection(raw_dataset, threshold, fs, duration)
        good_ix = np.delete(np.arange(0, old_epoched_data.get_data().shape[0], 1), ix)
        good_data = old_epoched_data.get_data()[good_ix, :, :]
        epoched_data = mne.EpochsArray(good_data, old_epoched_data.info)

        return epoched_data, good_ix

    else:
        epoched_data = old_epoched_data.get_data()[good_ix, :, :]

        return epoched_data

